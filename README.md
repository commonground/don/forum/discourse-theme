# Developer Overheid Discourse theme

Discourse theme for the forum of Developer Overheid.


## Working locally
Its possible to work on this theme locally. You can do this using the ruby gem `discourse_theme`.

### Prerequisites
- Ruby 2.2 or later

### Getting it to work

#### Create an API key in your Discourse instance
1. Go to the following URL: 
http://don-discourse.minikube/admin/api/keys
2. Click "Nieuwe API-sleutel"
3. For "Gebruikersniveau" select "Enkele gebruiker"
4. Select a user, in the case of a local instance you can use "discobot"
5. For the "Bereik" field, choose "Globaal"
6. Save the API and copy the API key in the next screen. Save the API key somewhere, you'll need it later.

#### Install and use gems watch

1. Install the gem:
```
gem install discourse_theme
```

2. Go to your theme folder
```
cd discourse-theme
```

3. Run the watcher gem
```
discourse_theme watch ./
```

4. When the CLI is prompting the URL, fill in local URL:
```
What is the root URL of your Discourse site? http://don-discourse.minikube
```

5. When the CLI is asking to store the site name, fill in the preferred option.
```
Would you like this site name stored in /home/tom/.discourse_theme? (Y/n) y
```

6. Fill in the API key you created earlier:
```
What is your API key? {{ FILL IN API KEY HERE}}
``` 

7. When the CLI is asking to store the API key, fill in the preferred option.
```
Would you like this API key stored in /home/tom/.discourse_theme? (Y/n) y
```

## Licence

Copyright © Stichting Geonovum 2023

[Licensed under the EUPLv1.2](LICENCE.md)
